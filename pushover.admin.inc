<?php

function pushover_admin_settings() {
  $form = array();

  $form['pushover_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Application API token'),
    '#default_value' => variable_get('pushover_api_token', NULL),
    '#required' => TRUE,
  );

  $form['pushover_user_key'] = array(
    '#type' => 'textfield',
    '#title' => t('User key to use when sending notifications'),
    '#default_value' => variable_get('pushover_user_key', NULL),
    '#required' => TRUE,
  );

  $types = array(PUSHOVER_NOTIFICATION_CURL => 'curl', PUSHOVER_NOTIFICATION_EMAIL => 'email');

  $form['pushover_notification_type'] = array(
    '#type' => 'radios',
    '#title' => t('Notification mechanism'),
    '#default_value' => variable_get('pushover_notification_type', PUSHOVER_NOTIFICATION_CURL),
    '#options' => $types,
    '#required' => TRUE,
  );

  $form['#validate'][] = 'pushover_admin_settings_validate';

  return system_settings_form($form);
}

function pushover_admin_settings_validate($form, &$form_state) {
  if($form_state['values']['pushover_notification_type'] == PUSHOVER_NOTIFICATION_EMAIL) {
    form_set_error('pushover_notification_type', 'Email API not yet supported');
  }
}

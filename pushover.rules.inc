<?php

/**
 * Implements hook_rules_action_info().
 */
function pushover_rules_action_info() {
  return array(
    'pushover_action_send_notification' => array(
      'label' => t('Send a notification via Pushover'),
      'arguments' => array(
        'msg' => array('type' => 'text', 'label' => t('The message to send')),
      ),
      'module' => 'pushover',
    ),
  );
}

function pushover_action_send_notification($msg) {
  $user_key = variable_get('pushover_user_key', NULL);

  if($user_key) {
    if(variable_get('pushover_notification_type', PUSHOVER_NOTIFICATION_CURL) == PUSHOVER_NOTIFICATION_CURL) {
      pushover_send_curl($user_key, $msg);
    }
  }
}
